"use strict";

import { Logger } from './Logger.js';
import { SteamAPI } from './SteamAPI.js';
import { TraderAPI } from './TraderAPI.js';
import { HealthChecker } from './HealthChecker.js';
import { TradeOfferManager } from './TradeOfferManager.js';
import { ExtensionManager } from './ExtensionManager.js';

const manifest = chrome.runtime.getManifest();

const trader = new TraderAPI(manifest.homepage_url);
const logger = new Logger('production',trader);
const steam = new SteamAPI(logger);
const health = new HealthChecker(logger, steam, trader, manifest.version);
const offers = new TradeOfferManager(logger, steam, trader, health);
const extension = new ExtensionManager(logger, offers);

trader.addLogger(logger);
trader.pingActiveState(health);