"use strict";

export class TraderAPI { 
	constructor(httpAddr) 
	{
		this.logger = {};
		this.httpAddr = httpAddr;
		this.pingingInterval = null;
	}
	async pingActiveState(health)
	{
		const httpAddr = this.httpAddr;
		clearInterval(this.pingingInterval);
		this._pingP2PChromeExtensionOnSite(httpAddr, health);
		this.pingingInterval = setInterval(() => {
			this._pingP2PChromeExtensionOnSite(httpAddr, health);
		}, 55000);
	}
	async _pingP2PChromeExtensionOnSite(httpAddr, health) {
	try
	{
		await health.check();

		if(health.allIsOk())
		{
			await fetch(`${ httpAddr }/api/pingP2PChromeExtension`);
		}
	}
	catch(err)
	{
		this.logger.error(err);
	}
}
	async isAuthedWithSteamid(steamid) 
	{
		let response = await fetch(`${ this.httpAddr }/api/getsteamid`);

		if(response.ok === false) {
			return false;
		}
		let json = await response.json(); 

		return json.success !== undefined && json.data == steamid;
	}
	async isSteamApiKeySaved(key) 
	{
		const response = await fetch(`${ this.httpAddr }/api/getUserSteamApiKey`);
		if(response.ok === false) {
			return false;
		}
		const json = await response.json(); 

		return json.success != null && json.data == key;
	}

	async saveSteamApiKeyOnTraderAccount(key)
	{
		const url = `${ this.httpAddr }/api/SetUserSteamApiKey/?steamApiKey=${key}`;

		try {
			const response = await fetch(url);

			const text = await response.text();
			
			const json = JSON.parse(text);

			return json.success;
		} catch(e) {
			this.logger.error(e.message);
			return false;
		}
	}

	async getLastExtensionVersion()
	{
		let response = await fetch(`${ this.httpAddr }/api/p2pTradesChromeExtensionLastVersion`);

		if(response.ok === false) {
			return false;
		}

		const json = await response.json();

		return json.version;
	}
	async notifyBackendAboutOfferCreatingResult(result) 
	{
		if(!result) return;
		let siteOrderIds = result.siteOrderIds;
		if(Array.isArray(siteOrderIds)) {
			siteOrderIds = JSON.stringify(siteOrderIds);
		}

		const url = this.httpAddr + '/api/OnP2POfferCreated/?siteOrderIds='+siteOrderIds+'&offerId='+result.tradeofferid+'&offerMsg='+result.message;
		const response = await fetch(url);
		const json = await response.json();
		return json;
	}
	async notifyBackendAboutOfferSendingResult(result) 
	{
		if(!result) return;
		const url = this.httpAddr + '/api/OnP2POfferSent/?siteOrderIds='+result.siteOrderIds+'&offerId='+result.tradeofferid
		const response = await fetch(url);

		return await response.json();
	}
	async notifyBackendAboutOfferChangedStatus(result) 
	{
		if(!result) return;
		const url = this.httpAddr + '/api/OnP2POfferChangedStatus/?offerId='+result.tradeofferid;
		const response = await fetch(url);

		return await response.json();
	}
	addLogger(logger) {
		this.logger = logger;
	}
	info(msg) {
		this._sendLog('info', msg);
	}
	error(msg) {
		this._sendLog('error', msg);
	}
	async _sendLog(level, msg) {
		const url = `${this.httpAddr}/api/P2PExtLog/?level=${level}&msg=${msg}`;
		const response = await fetch(url);
	}
}